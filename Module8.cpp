﻿#include <iostream>



class Stack
{

private:
	int a;
	int* Arr;
	int last_index = 0;
	
	

public:

	Stack(int N)
	{
		
		Arr = new int[N]; //выделение памяти в конструкторе
	}

	void push(int new_elem, int N)
	{
			
		if (last_index < N)
		{
			
			Arr[last_index++] = new_elem;
		}
	}


	void show(int N)
	{
		for (int i = 0; i < N; i++)
		{

			std::cout << Arr[i] << "\t";

		}
		std::cout << '\n';
	}

	int pop()
	{
		
		if (last_index > 0)
		{
						
			return Arr[last_index--];
		}
	}

	
	~Stack()
	{
		delete[] Arr; // удаление памяти в деструкторе
	}
};


int main()
{
	int N;
	std::cout << "Print N" << '\n';
	std::cin >> N;

	Stack s(N);

	int new_elem;
	std::cin >> new_elem;
	s.push(new_elem, N);

	s.show(N);

	s.pop();

	int b = s.pop();
}